package com.example.elhep_mms;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.Vector;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Agnieszka Zago�dzi�ska, Andrzej Woje�ski
 *
 */

public class MainActivity extends FragmentActivity { //implements WebSocketClientTokenListener {
	
	public final static int CONNECTION_PORT = 1900;
	public final static int WEBSOCKET_PORT = 7681;
	public final static String[] WEBSOCKET_PROTOCOL = {"elhep-test"};
	//public final static UUID BLUETOOTH_MMS_UUID = UUID.fromString("00000000-0000-0000-0000-11122223333");
	public final static UUID BLUETOOTH_MMS_UUID = UUID.fromString("00000000-0000-0000-0000-000000000000");
	public final static String DEBUG_SERVER = "UDP_CLIENT";
	
	DatagramSocket udp_socket;
	DatagramPacket data_udp_packet;		
	static TextView conn_label;
	// Connection server for SSDP discovery
	static ConnectionServer cs_;

	// Bluetooth
	private int REQUEST_ENABLE_BT = 1;
	// RESULT_OK - enabled
	// RESULT_CANCELED - disabled
	private int bluetooth_status;
	private mms_bluetooth _mms_bluetooth;
	private int bluetooth_on = 0; // status of Bluetooth adapter
	private int wifi_on = 0; // status of Wifi adapter
	
	// temp
	static Device_data data_temp;	
	
	static Context ApplicationContext;
	
	private static ArrayList<Device_data> DeviceArray = new ArrayList<Device_data>();
	private static ArrayList<BluetoothDevice> bluetoothArray = new ArrayList<BluetoothDevice>(); // just for checking existing Bluetooth devices

	static deviceAdapter _device_adapter;
	
	static Device_data actualDevice;
	
	ListView deviceList;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);
        conn_label = (TextView) findViewById(R.id.conn_status_txt);
        conn_label.setMovementMethod(new ScrollingMovementMethod());
        
        bluetoothArray.clear();
		DeviceArray.clear();
		bluetooth_on = 0;
		wifi_on = 0;
		ApplicationContext = getApplicationContext();
		
		ConnectivityManager connMgr = (ConnectivityManager) 
				getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		if (networkInfo != null && networkInfo.isConnected()) {
			conn_label.setText("Connected to network");			
			wifi_on = 1;
		} else {
			// display error
			conn_label.setText("Not connected to network");
			errorDialog err_dial_i = new errorDialog();
			err_dial_i.show(getSupportFragmentManager(), "Error");			
			// TODO exit app after closing
		}
				
		// Bluetooth management
		_mms_bluetooth = new mms_bluetooth(BluetoothAdapter.getDefaultAdapter());
		// Register the BroadcastReceiver for Bluetooth
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		registerReceiver(mReceiver, filter); // TODO Don't forget to unregister during onDestroy
		
		_device_adapter = new deviceAdapter(this, R.layout.device_list, DeviceArray);//(Device_data[]) DeviceArray.toArray());
		
		final Intent config_intent = new Intent(this, configScreen.class);
						
		deviceList = (ListView)findViewById(R.id.deviceList);
		deviceList.setAdapter(_device_adapter);
		deviceList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				// Make new screen with configuration of device
				//Bundle data = new Bundle();
				actualDevice = DeviceArray.get(position);
		    	startActivity(config_intent);
		    	
				//Toast.makeText(ApplicationContext, DeviceArray.get(position).IP, Toast.LENGTH_SHORT).show();
			}
			
		});
		
		// Check if bluetooth is available
		if (BluetoothAdapter.getDefaultAdapter() == null) {
			Log.d(DEBUG_SERVER, "Bluetooth disabled");			
		}
		else { // check if bluetooth is enabled
			if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {				
				// pop-up for turning on Bluetooth adapter
				/* working
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
				*/
			} // Bluetooth on - enable button
			else {
				//((Button)findViewById(R.id.bluetooth_btn)).setEnabled(true);
				bluetooth_on = 1;
			}
		}
			
		
		/** SSDP UDP SEARCH METHOD **/
		conn_label.append("\nStarting SSDP discovery server...\n");
		
		cs_ = new ConnectionServer(h_);
		cs_.start();
		/**  END SSDP UDP SEARCH METHOD **/
    }
    
    
    
    @Override
	protected void onPause() {
		super.onStop();		
		cs_.stop_server();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    // View data from ConnectionServer thread
    // manages data got from WebSocket connections
 	static final Handler h_ = new Handler() {
 		@Override
 		public void handleMessage(Message msg)	{			
 			
 			if (msg.arg1 == Websocket_connection.ARG_WS_NEW_DATA) { // got new data
 				// update interface
	 			Device_data data = (Device_data) msg.obj;
	 			conn_label.append("\nIP: " + data.IP + "\n");
	 			conn_label.append("Data: " + data.JSON_data.toString() + "\n");
	 			
	 			// check if this is unique device (example arg1 new connection or implement map)	 			
	 			// add data to array
	 			if (!DeviceArray.contains(data)) {
	 				Toast.makeText(ApplicationContext, "Found new Wifi device!", Toast.LENGTH_SHORT).show();
	 				DeviceArray.add(data);
	 				_device_adapter.notifyDataSetChanged();
	 			}
	 			// just update JSON file (check screen which should be used depending on data)
	 				 			
	 			data_temp = data;
 			}
 			else if (msg.arg1 == Websocket_connection.ARG_WS_CLOSED_CONN) { // closed connection
 				Device_data data = (Device_data) msg.obj;
 				conn_label.append("\nConnection closed with IP: " + data.IP + "\n"); // update array
 				// close connection and remove from list
 				cs_.closeWebsocket(data);
 				
 			}	
 			else if (msg.arg1 == Websocket_connection.ARG_WS_CLOSED_CONN) { // new data from bluetooth
 				String data = ((byte[])msg.obj).toString();
 				conn_label.append("\nGot data from Bluetooth: " + data + "\n");
 			}
 		} 		
 	};
 	 /** Called when the user selects the Websocket test button */
    public void button1(View view) {
    	
    	// send last received data
    	cs_.sendDataToDevice(data_temp);
    	
    }
    
    /** Called when the user selects the Search Wifi button */
    public void btn_scan_network(View view) {
    	
    	// disconnect with all devices or not clear array (must disconnect with Wifi Websockets)?
    	
    	DeviceArray.clear(); 
    	bluetoothArray.clear();
    	_device_adapter.notifyDataSetChanged();    
    	
    	if (wifi_on == 1) {
    		cs_.ssdp_search();
    		Toast.makeText(getApplicationContext(), "Wifi scanning started!", Toast.LENGTH_SHORT).show();
    	}
    	
    	if (bluetooth_on == 1) {
    		_mms_bluetooth.scanBluetooth();
    		Toast.makeText(getApplicationContext(), "Bluetooth scanning started!", Toast.LENGTH_SHORT).show();
    	}
    	
        // Do something in response to button
    	//Intent intent = new Intent(this, ServerActivity.class);
    	//EditText editText = (EditText) findViewById(R.id.edit_message);
    	//String message = editText.getText().toString();
    	//intent.putExtra(EXTRA_MESSAGE, message);
    	//startActivity(intent);
    }
    
    /** Called when the user selects the Search Bluetooth button */
    public void btn_scan_bluetooth(View view) {
    	// disconnect from Bluetooth MMS devices
    	// do bluetooth device discovery
    	_mms_bluetooth.scanBluetooth();
    	// TODO pop-up
    	
    }
    
    // getting data from popup (Bluetooth enable) - same for enable Wifi
    /*
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	if (requestCode == REQUEST_ENABLE_BT)
    		
    		bluetooth_status = resultCode;
    	
    		if (resultCode == RESULT_OK) {    			
    			// got data
    			Log.d(DEBUG_SERVER, "OK Got data" + resultCode);
    			((Button)findViewById(R.id.bluetooth_btn)).setEnabled(true);
    		}
    		else
    			Log.d(DEBUG_SERVER, "No Got data" + resultCode);
    }
    */
    
    // Create a BroadcastReceiver for ACTION_FOUND (Bluetooth discovery)
 	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
 	    public void onReceive(Context context, Intent intent) {
 	        String action = intent.getAction();
 	        // When discovery finds a device
 	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
 	        	// Get the BluetoothDevice object from the Intent
 	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
 	            // Add the name and address to an array adapter to show in a ListView 	 
 	           if (!bluetoothArray.contains(device)) {
	 				bluetoothArray.add(device);
	 				Device_data device_ = new Device_data();
	 				device_.handler = device;
	 				device_.IP = device.getAddress();
	 				device_.JSON_data = null;
	 				device_.type = Device_data.CONNECTION_BLUETOOTH;
	 				
	 				DeviceArray.add(device_);	 		
	 				
	 				Toast.makeText(getApplicationContext(), "Found new Bluetooth device!", Toast.LENGTH_SHORT).show();
	 			}
 	            
 	            /*
 	            if ("bluetooth_device_name".equals(device.getName()))
 	            	bluetoothArray.add(device);
 	            */
 	            conn_label.append("Bluetooth device:\nName: " + device.getName() + "\nAddr: " + device.getAddress() + "\n"); 	            		
 	        } 
 	        else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
 	        	// TODO show pop-up scanning done 
 	        	
 	        	// show device list (update screen)
 	        	Toast.makeText(getApplicationContext(), "Bluetooth scanning finished!", Toast.LENGTH_SHORT).show();
 	        	_device_adapter.notifyDataSetChanged();
 	        	
 	        	//_mms_bluetooth.connectWithDevice(bluetooth_vector.firstElement()); // beaglebone 	        
 	        	//_mms_bluetooth.stopBluetoothDiscovery();
 	        }
 	    }
 	};
    
}
