package com.example.elhep_mms;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class mms_bluetooth_device extends Thread {
	
	private final BluetoothSocket _bt_socket;
	private final InputStream _bt_input;
	private final OutputStream _bt_output;
	
	public final static int ARG_BLUETOOTH_NEW_DATA = 3;	
	
	// handler to main thread message queue
	private final Handler handler_;
	
	public mms_bluetooth_device(BluetoothSocket socket) {
		
		InputStream tmp_bt_input = null;
		OutputStream tmp_bt_output = null;
		
		handler_ = MainActivity.h_;		
		_bt_socket = socket;
				
		try {
			tmp_bt_input = _bt_socket.getInputStream();
			tmp_bt_output = _bt_socket.getOutputStream();
		} catch (IOException e) {
			// error popup
		}
			
		_bt_input = tmp_bt_input;
		_bt_output = tmp_bt_output;
		
		Log.d("Bluetooth", "Sending test data");
		write("test data".getBytes());
		
	}
	
	public void run() {
		byte[] buffer = new byte[1024];
		//int bytes;
	
		// TODO use bufferreader
		while(true) {
			
			try {
				// read data from stream
				_bt_input.read(buffer);				
				// send to main thread
				Message msg = handler_.obtainMessage();
				msg.arg1 = ARG_BLUETOOTH_NEW_DATA;
				msg.obj = buffer;
				handler_.sendMessage(msg);
				
			} catch (IOException e) {
				break;
			}			
			
		}
	}
	
	public void write(byte[] bytes) {
		
		try {
			_bt_output.write(bytes);
		} catch (IOException e) {
			// TODO popup error
		}
	}
	
	public void cancel() {
		
		try {
			_bt_socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
