package com.example.elhep_mms;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * @author Agnieszka Zago�dzi�ska, Andrzej Woje�ski
 *
 */

public class ServerActivity extends Activity {	
	
	DatagramSocket udp_socket;
	DatagramPacket data_udp_packet;		
	static TextView conn_label;
	ConnectionServer cs_;	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {	
		
		super.onCreate(savedInstanceState);		
		
		String connection_status;
		
		setContentView(R.layout.test_screen);
		
		ConnectivityManager connMgr = (ConnectivityManager) 
				getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		if (networkInfo != null && networkInfo.isConnected()) {
			connection_status = "Connected";			
		} else {
			// display error
			connection_status = "Not connected";
		}			
		
		/** THIS IS FOR SSDP UDP SEARCH METHOD **/		
		conn_label = (TextView) findViewById(R.id.conn_status_txt);		
		conn_label.setText(connection_status);  			
		
		conn_label.append("\nStarting UDP Server...");
		
		cs_ = new ConnectionServer(h_);
		cs_.start();	
	
		//try {
		//	Thread.sleep(1000);
		//} catch (InterruptedException e) {
		//	Log.d(MainActivity.DEBUG_CLIENT, "SLEEP_INTERRUPT_EXCEPTION");
		//}	
		
		/** END -- THIS IS FOR SSDP UDP SEARCH METHOD **/
		
		// Websocket connection

		// Connect to host

		// Start Websocket connection

		// Download JSON config

		// Print data
		
		// Create the text view
		/*		TextView textView = new TextView(this);
				textView.setTextSize(40);
				textView.setText(connection_status);	
				
		setContentView(textStatus); 
		
		// Get the message from the intent
		Intent intent = getIntent();
		String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

		// Create the text view
		TextView textView = new TextView(this);
		textView.setTextSize(40);
		textView.setText(message);

		setContentView(textView);
		*/

	}
	
	@Override
	protected void onPause() {
		super.onStop();		
		cs_.stop_server();
	}
	
	public void btn_scan_network(View view) {
		cs_.ssdp_search();
	}
	
	// View data from ConnectionServer thread
	static final Handler h_ = new Handler() {
		@Override
		public void handleMessage(Message msg)	{			
			Bundle b = msg.getData();
			
			conn_label.append("\nNew Device:\nInfo:\n");
			conn_label.append("PACKET_DATA: " + b.getString("PACKET_DATA") + "\n");
			conn_label.append("PACKET_LENGTH: " + b.getInt("PACKET_LENGTH") + "\n");
			conn_label.append("SENDER_IP: " + b.getString("SENDER_IP") + "\n");
			
		}
		
	};

}
