/*
 *  * listener.c -- joins a multicast group and echoes all data it receives from
 *   *		the group to its stdout...
 *    *
 *     * Antony Courtney,	25/11/94
 *      * Modified by: Frédéric Bastien (25/03/04)
 *       * to compile without warning and work correctly
 *        */

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <string.h>
#include <stdio.h>


#define HELLO_PORT 1900
#define HELLO_GROUP "239.255.255.250"
#define MSGBUFSIZE 256
#define RFC1123FMT "%a, %d %b %Y %H:%M:%S GMT"

void dump_to_hex(char * buf) {
	int i;
	printf("\n");
	int len = strlen(buf);
	for(i=0; i<len; i++) printf("%02X", buf[i]);
	printf("\n");
}

void do_process_search(char * buf, int buflen, int socket, struct sockaddr_in * addr, int addrlen){
	int pos = 0;
	char *buffer = NULL, *aPtr ; buffer = strdup(buf);
	unsigned char do_response = 0;
	do {
		aPtr = strsep(&buffer, "\n\r");
		if (!aPtr) continue;
		if (aPtr[0] == '\r' || aPtr[0] == '\n') aPtr++;
		if (aPtr[0] == 0) continue;
		if (strncmp(aPtr, "ST:urn:ELHEP:device:MMS:", 24) == 0){
			do_response=1;
		} else {
		}
	} while(aPtr);
	if ( do_response) {
		time_t now;
		char timebuf[128];
		char resp_buffer[1024];

//  fprintf(f, "%s %d %s\r\n", PROTOCOL, status, title);
//  fprintf(f, "Server: %s\r\n", SERVER);
		now = time(NULL);
		strftime(timebuf, sizeof(timebuf), RFC1123FMT, gmtime(&now));
	
		snprintf(resp_buffer, 1024, "HTTP/1.1 200 OK\r\nCACHE-CONTROL: max-age = 60\r\nDATE: %s\r\nEXT:\r\nLOCATION: 127.0.0.1\r\nSERVER: Linux/2.6 UPnP/1.1 MMS/0.1\r\nST: urn:ELHEP:device:MMS:0.1\r\nUSN: 13770dbd-a652-4e98-a353-e0cfe09a18ae::\r\n\r\n", timebuf);
		int result = sendto(socket, resp_buffer, strlen(resp_buffer), 0, (struct sockaddr *) addr, addrlen);
		printf("Send response (%d)\n",result);
	}
}

main(int argc, char *argv[])
{
     struct sockaddr_in addr;
     int fd, nbytes,addrlen;
     struct ip_mreq mreq;
     char msgbuf[MSGBUFSIZE];
	char *message="Hello, World!";


     u_int yes=1;            /*** MODIFICATION TO ORIGINAL */

     /* create what looks like an ordinary UDP socket */
     if ((fd=socket(AF_INET,SOCK_DGRAM,0)) < 0) {
	  perror("socket");
	  exit(1);
     }


/**** MODIFICATION TO ORIGINAL */
    /* allow multiple sockets to use the same PORT number */
    if (setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
       perror("Reusing ADDR failed");
       exit(1);
       }
/*** END OF MODIFICATION TO ORIGINAL */

     /* set up destination address */
     memset(&addr,0,sizeof(addr));
     addr.sin_family=AF_INET;
     addr.sin_addr.s_addr=htonl(INADDR_ANY); /* N.B.: differs from sender */
     addr.sin_port=htons(HELLO_PORT);
     
     /* bind to receive address */
     if (bind(fd,(struct sockaddr *) &addr,sizeof(addr)) < 0) {
	  perror("bind");
	  exit(1);
     }
     
     /* use setsockopt() to request that the kernel join a multicast group */
     mreq.imr_multiaddr.s_addr=inet_addr(HELLO_GROUP);
     mreq.imr_interface.s_addr=htonl(INADDR_ANY);
     if (setsockopt(fd,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) {
	  perror("setsockopt");
	  exit(1);
     }

     /* now just enter a read-print loop */
     while (1) {
	  addrlen=sizeof(addr);
	  if ((nbytes=recvfrom(fd,msgbuf,MSGBUFSIZE,0,
			       (struct sockaddr *) &addr,&addrlen)) < 0) {
	       perror("recvfrom");
	       exit(1);
	  }
	  msgbuf[nbytes] = 0;
	  if (strncmp (msgbuf,"M-SEARCH * HTTP/1.1", 8 ) == 0) {
		do_process_search(msgbuf, nbytes, fd, &addr, addrlen);
	  } else { 
		puts("Unknown message\n");
		puts(msgbuf);
          }
		
     }
}
