//
//  Device+Utility.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Device+Utility.h"
#import "Probe+Utility.h"

@implementation Device (Utility)

- (NSString *)description{
    
    NSMutableString *uniqueProbes = [[NSMutableString alloc] initWithCapacity:0];
    
    for (Probe *probe in self.uniqueProbes) {
        [uniqueProbes appendFormat:@"%@", [probe description]];
    }
    
    return [NSString stringWithFormat:@"deviceID:%@,\ndeviceName: %@,\nverID: %@,\ndateAdded: %@,\nuniqueProbes :%@",
            [self.deviceID description], self.deviceName, [self.verID description],[self.dateAdded description],uniqueProbes];
}

- (NSDictionary *)configuration{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [dictionary setObject:self.deviceID forKey:@"deviceID"];
    
    NSArray *uniqueProbes = [self uniqueProbesArray];
    
    [dictionary setObject:uniqueProbes forKey:@"uniqueProbes"];
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (NSArray *)uniqueProbesArray{
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:self.uniqueProbes.count];
    
    for (Probe *probe in self.uniqueProbes) {
        [array addObject:[probe configuration]];
    }
    
    return [NSArray arrayWithArray:array];
}

@end
