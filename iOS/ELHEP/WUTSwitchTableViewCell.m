//
//  WUTSwitchTableViewCell.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 7/18/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTSwitchTableViewCell.h"

#define SWITCH_FRAME CGRectMake(233, 10, 79, 27)

@implementation WUTSwitchTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.slider = [[UISwitch alloc] initWithFrame:SWITCH_FRAME];
        _slider.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        
        [self.contentView addSubview:_slider];
        
    }
    return self;
}

- (void)switchChanged:(UISwitch *)aSwitch{
    
}

- (void)addTargetToSwitch:(id)target action:(SEL)selector{
    [_slider addTarget:target action:selector forControlEvents:UIControlEventValueChanged];
}


@end
