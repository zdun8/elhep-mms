//
//  Device.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/21/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Device.h"
#import "Probe.h"


@implementation Device

@dynamic address;
@dynamic dateAdded;
@dynamic deviceID;
@dynamic deviceName;
@dynamic interface;
@dynamic verID;
@dynamic isConnected;
@dynamic uniqueProbes;

@end
