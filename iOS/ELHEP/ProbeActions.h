//
//  ProbeActions.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe, ProbeAction;

@interface ProbeActions : NSManagedObject

@property (nonatomic, retain) NSSet *actions;
@property (nonatomic, retain) Probe *probe;
@end

@interface ProbeActions (CoreDataGeneratedAccessors)

- (void)addActionsObject:(ProbeAction *)value;
- (void)removeActionsObject:(ProbeAction *)value;
- (void)addActions:(NSSet *)values;
- (void)removeActions:(NSSet *)values;

@end
