//
//  Measurement.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/21/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Measurement.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@implementation Measurement

- (id)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(!self)return nil;
    
    if (![dictionary isKindOfClass:[NSDictionary class]]) {
        NSLog(@"not a dict!");
        return nil;
    }
    
    NSArray *samples = dictionary[@"probeValue"];
    
    _sample = [samples[0] floatValue];
    
    NSNumber *timeStamp = dictionary[@"timestamp"];
    DDLogVerbose(@"time stamp from dictionary:%@", timeStamp);
    _timestamp = [timeStamp doubleValue];
    
    return self;
}

+ (Measurement *)measurementWithDict:(NSDictionary *)dict{
    return [[Measurement alloc] initWithDictionary:dict];
}

@end
